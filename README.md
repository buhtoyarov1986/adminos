# Adminos

## Installation

Install initial adminos-structured files with

    $ rails g adminos:install

To add locales to installation:

    $ rails g adminos:install --locales=en,cn

Russian language is added and made the default one by default.

## Model generation

Generate adminos model with

    $ rails g adminos Model field:string body:text --type=sortable

Name, slug, published, meta_description and meta_title fields are always added.

Available model types are: default(if no type is present), section, sortable and table.

For section type, in you model must defined MAX_DEPTH constant (3 by default).

Add sorting to field:

    $ rails g adminos Model field:string:sort --type=table

Type table is required for sorting to function properly. Sorting by name is added by default.

Add search form:

    $ rails g adminos Model body:text --search=name,body

Specify the fields to search in. It is performed using pg_search gem and directed towards quick word-based search solution, but can be customized further.

If the model will be in multiple locales, you can specify which fields to translate:

    $ rails g adminos Model field:string:locale body:text:locale

To translate default fields in case no locale specific custom fields are present, add --locale option:

    $ rails g adminos Model --locale

If no need in SEO fields, just type --seo=no when you generate model.

    $ rails g adminos Model --seo=no

## Test

    $ rake test .

## Example Simple Form CarrierWave input

```ruby
  f.input :file, as: :carrierwave
  f.input :image, as: :carrierwave, input_html: { image: true, preview_version: :thumb}
  # preview_version == :preview by default
```

## Default editor (Redactor-rails)

https://github.com/SammyLin/redactor-rails

If you need change some config in redactor, you can

    $ rails generate redactor:config

Standart list extension whitelist

    $ RedactorRails.document_file_types
    # ["pdf", "doc", "docx", "xls", "xlsx", "rtf", "txt"]

You can override the Uploaders

Keep it here - `/app/uploaders/redactor_rails_document_uploader.rb`

```ruby
class RedactorRailsDocumentUploader < CarrierWave::Uploader::Base
  include RedactorRails::Backend::CarrierWave

  # storage :fog
  storage :file

  def store_dir
    "system/redactor_assets/documents/#{model.id}"
  end

  def extension_white_list
    RedactorRails.document_file_types << 'zip'
  end
end

```
or

```ruby
# encoding: utf-8
class RedactorRailsPictureUploader < CarrierWave::Uploader::Base
  include RedactorRails::Backend::CarrierWave

  # Include RMagick or ImageScience support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick
  # include CarrierWave::ImageScience

  # Choose what kind of storage to use for this uploader:
  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "system/redactor_assets/pictures/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  process :read_dimensions

  # Create different versions of your uploaded files:
  version :thumb do
    process :resize_to_fill => [118, 100]
  end

  version :content do
    process :resize_to_limit => [800, 800]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    RedactorRails.image_file_types
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
end

```


## Deploy the application

#### Prepare config files.

    $ cap staging deploy:setup_config

Path for tasks - `/lib/capistrano/tasks/`

Path for config templates - `/config/deploy/shared`

#### Settings `deploy.rb`

    set(:config_files, %w(
      config/database.yml
      config/unicorn.rb
      .env
      config/nginx.conf
    ))

    set(:symlinks, [
      {
        source: "nginx.conf",
        link: "/etc/nginx/sites-enabled/{{app_name}}"
      }
    ])
