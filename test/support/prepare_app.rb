require 'pathname'
require 'bundler'

module Test
  module PrepareApp
    module_function

    def system!(*args)
      ENV['DISABLE_SPRING'] = '1'
      # https://github.com/bundler/bundler/issues/1424
      Bundler.with_clean_env do
        ENV['DISABLE_SPRING'] = '1'
        puts "Running: #{args.join(' ')}"
        system(*args) || abort("\n== Command #{args} failed ==")
      end
    end

    def generate_rails_application(path, force: true)
      path = Pathname(path)
      path.rmtree if force && path.exist?
      path.dirname.mkpath
      system!("rails new #{path} --no-rc -d postgresql")
    end

    def within_rails_application(path, **options)
      generate_rails_application(path, **options)
      Dir.chdir(path) { yield }
    end

    def generate_adminos_application(path, force: true, locales: false)
      locales = locales.join(',') if locales.is_a?(Array)
      within_rails_application(path, force: force) do
        system! %(echo "gem 'adminos', path: '../../../adminos'" >> Gemfile)
        system! 'bundle'
        generate %(adminos:install --is-test=true --force#{" --locales=#{locales}" if locales})
      end
      path
    end

    def generate(command)
      system! %(bin/rails generate #{command})
    end
  end
end
