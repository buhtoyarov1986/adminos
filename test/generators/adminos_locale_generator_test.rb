require 'test_helper'

class AdminosLocaleGeneratorTest < Rails::Generators::TestCase
  include Test::PrepareApp

  destination DUMMY_LOCALE

  def self.prepare_destination
    @destination_prepared ||= Test::PrepareApp.generate_adminos_application(DUMMY_LOCALE, locales: 'en,nl')
  end

  setup { self.class.prepare_destination }

  test 'Assert locale generation' do
    name = 'event_locale'

    unless DUMMY_LOCALE.join("app/models/#{name}.rb").file?
      Dir.chdir(DUMMY_LOCALE) do
        generate "adminos #{name.classify} --locale"
      end
    end

    assert_file "app/views/admin/#{name.pluralize}/_locale_fields.slim"
    assert_file "app/views/admin/#{name.pluralize}/_general_fields.slim"

    assert_file "app/models/#{name}.rb"
    assert_file 'config/routes.rb', /resources :#{name.pluralize}, except: :show/
    assert_file 'app/views/shared/admin/_sidebar.slim', /= top_menu_item active: 'admin\/#{name.pluralize}\#'/
    assert_file 'config/locales/adminos.ru.yml', /#{name.pluralize}:/
    assert_file "app/controllers/admin/#{name.pluralize}_controller.rb"
    assert_directory "app/views/admin/#{name.pluralize}"
  end
end
