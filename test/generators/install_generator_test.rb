require 'test_helper'

class InstallGeneratorTest < Rails::Generators::TestCase
  destination DUMMY

  def self.prepare_destination
    @destination_prepared ||= Test::PrepareApp.generate_adminos_application(DUMMY)
  end

  setup { self.class.prepare_destination }

  test 'Assert models' do
    assert_file 'app/models/user.rb'
    assert_file 'app/models/page.rb'
    assert_file 'app/models/ability.rb'
    assert_file 'app/models/settings.rb'
    assert_file 'app/models/authentication.rb'
  end

  test 'Assert controllers' do
    assert_file 'app/controllers/pages_controller.rb'
    assert_file 'app/controllers/application_controller.rb'
    assert_file 'app/controllers/authentications_controller.rb'
    assert_file 'app/controllers/admin/base_controller.rb'
    assert_file 'app/controllers/admin/helps_controller.rb'
    assert_file 'app/controllers/admin/pages_controller.rb'
    assert_file 'app/controllers/admin/settings_controller.rb'
    assert_file 'app/controllers/admin/users_controller.rb'
    assert_file 'app/controllers/admin/versions_controller.rb'
    assert_file 'app/controllers/admin/users_controller.rb'
  end

  test 'Assert views' do
    assert_directory 'app/views/admin'
    assert_directory 'app/views/admin/base'
    assert_directory 'app/views/admin/helps'
    assert_directory 'app/views/admin/pages'
    assert_directory 'app/views/admin/settings'
    assert_directory 'app/views/admin/versions'
    assert_directory 'app/views/admin/users'
    assert_directory 'app/views/authentications'
    assert_directory 'app/views/devise'
    assert_directory 'app/views/kaminari'
    assert_directory 'app/views/layouts'
    assert_directory 'app/views/shared'
    assert_file 'app/views/pages/show.slim'
  end

  test 'Assert config' do
    assert_file '.ruby-version', /#{RUBY_VERSION}/
    assert_file 'config/initializers/carrierwave.rb'
    assert_file 'config/initializers/simple_form.rb'
    assert_file 'config/initializers/kaminari_config.rb'
    assert_file 'config/locales/devise.ru.yml'
    assert_file 'config/locales/adminos.ru.yml'
    assert_file 'config/routes.rb'
    assert_file 'config/application.rb', /config.time_zone = 'Moscow'/
    assert_file 'config/application.rb', /config.i18n.default_locale = :ru/
  end

  test 'Assert mailers' do
    assert_file 'app/mailers/notifier.rb'
  end

  test 'Assert uploaders' do
    assert_file 'app/uploaders/common_uploader.rb'
  end

  test 'Assert inputs' do
    assert_file 'app/inputs/carrierwave_input.rb'
  end

  test 'Assert assets' do
    assert_file 'app/assets/stylesheets/admin/application.scss'
    assert_file 'app/assets/javascripts/admin/application.js'
  end

  test 'Assert deploy' do
    assert_file 'config/deploy.rb'
    assert_file 'config/deploy/staging.rb'
  end

  test 'Assert general' do
    assert_file 'public/404.html'
    assert_file 'public/500.html'
    assert_file 'Capfile'
    assert_file 'Gemfile'
  end
end
