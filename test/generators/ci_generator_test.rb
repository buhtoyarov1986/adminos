require 'test_helper'

class CiGeneratorTest < Rails::Generators::TestCase
  include Test::PrepareApp

  CI_APP = GENERATED.join('ci')

  destination CI_APP

  def self.prepare_destination
    @destination_prepared ||= generate_rails(CI_APP) { generate 'adminos:ci' }
  end

  setup { self.class.prepare_destination }

  test 'Assert CI' do
    assert_file '.gitlab-ci.yml'
  end

  test 'Assert spec' do
    assert_file 'Gemfile', /gem 'rspec-rails'/
    assert_file 'bin/rspec'
    assert_file '.gitlab-ci.yml', /^spec:/
  end

  test 'Assert audit' do
    assert_file 'Gemfile', /gem 'bundler-audit'/
    assert_file 'bin/bundler-audit'
    assert_file 'rakelib/audit.rake'
    assert_file '.gitlab-ci.yml', /^audit:/
  end

  test 'Assert lint' do
    assert_file 'Gemfile', /gem 'rubocop'/
    assert_file 'bin/rubocop'
    assert_file 'rakelib/lint.rake'
    assert_file '.rubocop.yml'
    assert_file '.rubocop_todo.yml'
    assert_file '.gitlab-ci.yml', /^lint:/
  end
end
