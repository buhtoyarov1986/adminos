require 'test_helper'

class InstallLocaleGeneratorTest < Rails::Generators::TestCase
  destination DUMMY_LOCALE

  def self.prepare_destination
    @destination_prepared ||= Test::PrepareApp.generate_adminos_application(DUMMY_LOCALE, locales: 'en,nl')
  end

  setup { self.class.prepare_destination }

  test 'Assert models' do
    assert_file 'app/models/page.rb', /translates :name, .*/
    assert_file 'app/models/page.rb', /accepts_nested_attributes_for :translations/
  end

  test 'Assert controllers' do
    assert_file 'app/controllers/admin/pages_controller.rb', /filter_by_locale: true,/
    assert_file 'app/controllers/application_controller.rb', /, :set_locale/
    assert_file 'app/controllers/application_controller.rb', /def default_url_options/
  end

  test 'Assert views' do
    assert_file 'app/views/admin/base/_fields.slim'
    assert_file 'app/views/admin/base/_pills.slim'
    assert_file 'app/views/admin/pages/_general_fields.slim'
    assert_file 'app/views/admin/pages/_locale_fields.slim'
    assert_file 'app/views/shared/admin/_sidebar.slim', /I18n.available_locales.each do |locale|/
  end

  test 'Assert config' do
    assert_file 'config/initializers/globalize_fields.rb'
    assert_file 'config/routes.rb', /scope '\(:locale\)'/
    assert_file 'config/application.rb', /config.i18n.available_locales = \[:ru, :en, :nl\]/
  end

  test 'Assert Gemfile' do
    assert_file 'Gemfile', /gem 'globalize'/
  end
end
