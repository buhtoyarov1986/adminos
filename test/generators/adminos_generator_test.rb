require 'test_helper'

class AdminosGeneratorTest < Rails::Generators::TestCase
  include Test::PrepareApp

  destination DUMMY

  def self.prepare_destination
    @destination_prepared ||= Test::PrepareApp.generate_adminos_application(DUMMY)
  end

  setup { self.class.prepare_destination }

  %w(default section sortable table).each do |type|
    name = "event_#{type}"

    test "Assert generation of #{type} event #{name}" do

      unless File.exist? "#{DUMMY}/app/models/#{name}.rb"
        Dir.chdir(DUMMY) do
          generate "adminos #{name.classify} --type=#{type}"
        end
      end

      assert_file "app/models/#{name}.rb"
      assert_file 'config/routes.rb', /resources :#{name.pluralize}, except: :show/
      assert_file 'app/views/shared/admin/_sidebar.slim', /= top_menu_item active: 'admin\/#{name.pluralize}\#'/
      assert_file 'config/locales/adminos.ru.yml', /#{name.pluralize}:/
      assert_file "app/controllers/admin/#{name.pluralize}_controller.rb"
      assert_file "app/views/admin/#{name.pluralize}/_fields.slim"
      assert_file "app/views/admin/#{name.pluralize}/index.slim"
    end
  end
end
