require 'rails'
require 'adminos'
require 'rails/test_help'
require 'rails/generators/test_case'
require 'pathname'
require 'fileutils'
require_relative 'support/prepare_app'


DUMMY = Pathname(__dir__).join('dummy')
DUMMY_LOCALE = Pathname(__dir__).join('dummy_with_locale')

def create_dummy
  generate_adminos_application(DUMMY)
end

def create_dummy_with_locale
  generate_adminos_application(DUMMY_LOCALE, locales: 'en,nl')
end

ENV['RAILS_ENV'] = 'test'
