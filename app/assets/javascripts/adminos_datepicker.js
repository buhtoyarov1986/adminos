//= require bootstrap-datepicker
//= require bootstrap-datepicker/locales/bootstrap-datepicker.ru.js

$(function() {
  initDatePicker();
  //setDate();
})

function initDatePicker() {
  $('.date-picker').datepicker({
    format: "yyyy-mm-dd",
    language: "ru",
    //calendarWeeks: true,
    todayHighlight: true,
    startDate: '1999-01-01',
    endDate: '+1m'
  })
  .on('changeDate', function(e) {
    $('.input-date-field input').val(e.date)
  });
}

function setDate() {
  var postDate = $('.input-date-field input').val();
  $('.date-picker').datepicker('setDate', postDate);
}

$(window).on('turbolinks:load', function() {
  initDatePicker();
  setDate();
});
