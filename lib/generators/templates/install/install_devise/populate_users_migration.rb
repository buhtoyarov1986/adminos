class PopulateUsers < ActiveRecord::Migration[5.1]
  def self.up
    User.create email: 'studio@molinos.ru', password: 'changeme', roles: [:admin]
  end

  def self.down
    User.find_by_email('studio@molinos.ru').delete
  end
end
