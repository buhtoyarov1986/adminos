module ShortcutMacros
  def create(type, *args)
    FactoryBot.create type, *args
  end

  def build(type, *args)
    FactoryBot.build type, *args
  end
end
