window.init_redactor = function () {
  $(function () {
    var csrf_token = $('meta[name=csrf-token]').attr('content');
    var csrf_param = $('meta[name=csrf-param]').attr('content');
    var params = {};
    params[csrf_param] = csrf_token;
    // Set global settings
    $.Redactor.settings = {
      plugins: ['table', 'imagemanager', 'video', 'filemanager', 'fullscreen'],
      imageUpload: '/redactor_rails/images',
      imageUploadFields: params,
      fileUpload: '/redactor_rails/files',
      fileUploadFields: params,
      buttons: [
        'format', 'bold', 'italic', 'deleted', 'lists',
        'image', 'file', 'link', 'horizontalrule'
      ],
      lang: 'ru',
    };
    // Initialize Redactor
    $('.redactor').redactor();
  });
};
$(document).on('turbolinks:load', window.init_redactor);
