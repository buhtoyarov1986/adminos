window.init_redactor = function () {
  var csrf_token = $('meta[name=csrf-token]').attr('content');
  var csrf_param = $('meta[name=csrf-param]').attr('content');
  var params = {};
  if (csrf_param !== undefined && csrf_token !== undefined) {
    params[csrf_param] = csrf_token;
  }
  $('.redactor').redactor({
    // You can specify, which ones plugins you need.
    // If you want to use plugins, you have add plugins to your
    // application.js and application.css files and uncomment the line below:
    // 'plugins': ['fontfamily', 'fontsize', 'fontcolor', 'table', 'imagemanager', 'video', 'filemanager', 'fullscreen'],
    plugins: ['table', 'imagemanager', 'video', 'filemanager', 'fullscreen'],
    imageUpload: '/redactor_rails/pictures',
    imageUploadErrorCallback: function (json) {
      alert(json.error.data[1]);
    },
    fileUploadErrorCallback: function (json) {
      alert(json.error.data[1]);
    },
    uploadImageFields: params,
    imageGetJson: '/redactor_rails/pictures',
    fileUpload: '/redactor_rails/documents',
    uploadFileFields: params,
    fileGetJson: '/redactor_rails/documents',
    path: '/assets/redactor-rails',
    buttons: [
      'formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'outdent', 'indent',
      'alignment', 'link', 'table', 'image', 'video', 'file', 'html', 'fullscreen'
    ],
    convertLinks: false,
    convertUrlLinks: false,
    convertImageLinks: false,
    convertVideoLinks: false,
    lang: 'ru'
  });
}

$(document).on('turbolinks:load', window.init_redactor);
