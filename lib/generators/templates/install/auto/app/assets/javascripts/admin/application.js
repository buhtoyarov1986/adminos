//= require jquery
//= require jquery_ujs

//= require admin/tether.min
//= require admin/bootstrap.min

//= require jquery.simplecolorpicker.js
//= require jquery.autosize

//= require admin/jquery.optiscroll.min

//= require turbolinks

//= require adminos_base
//= require adminos_select
//= require adminos_datepicker

//= require redactor_rails/config

//= require_self
