class SvgPreviewUploader < CommonUploader
  include CarrierWave::MiniMagick

  version :preview do
    process convert: 'jpg'
    process resize_to_fit: [150, 150]

    def full_filename(for_file)
      super(for_file).chomp(File.extname(super(for_file))) + '.jpg'
    end
  end
end
