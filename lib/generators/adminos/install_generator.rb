require 'bundler'
require 'rails/generators/active_record'
require_relative '../helpers/gemfile_merge.rb'

module Adminos::Generators
  class InstallGenerator < ActiveRecord::Generators::Base
    desc 'Creates initial adminos file and data structure'

    source_root File.expand_path '../../templates/install', __FILE__

    # ActiveRecord::Generators::Base requires a NAME parameter.
    argument :name, type: :string, default: 'random_name'

    # options
    class_option :locales, type: :string
    class_option :is_test, type: :boolean, default: false

    def questions
      @settings = {}
      @settings[:install_devise] = ask_user('Install Devise?')
      @settings[:add_devise_views_locales] = @settings[:install_devise] || ask_user('Add Devise views (login page, etc..) and locales?')
      @settings[:add_users_to_admin_panel] = @settings[:install_devise] || ask_user('Add Devise Users to admin panel?')
      @settings[:add_authentications] = !@settings[:install_devise] || ask_user('Add Authentications for Users?')
      @settings[:install_paper_trail] = ask_user('Install PaperTrail?')
      @settings[:add_versions_to_admin_panel] = @settings[:install_paper_trail] || ask_user('Add PaperTrail versions to admin panel?')
      @settings[:add_pages] = ask_user('Create Pages model?')
      @settings[:create_redactor2_assets] = ask_user('Create_table :redactor2_assets ?')
      @settings[:set_default_locale_time_zone] = ask_user('Set default_locale = :ru and time_zone = "Moscow" ? in application.rb')
    end

    def merge_gemfile
      adminos_gf = file_content('Gemfile')
      project_gf = IO.read('Gemfile')
      merged_gf = GemfileMerge.new(adminos_gf, project_gf).merge
      remove_file 'Gemfile'
      create_file 'Gemfile', merged_gf
      append_file 'Gemfile', file_content('locale/Gemfile') if options.locales?
    end

    def auto
      directory 'auto', '.', mode: :preserve
    end

    def migrations
      migration_template 'settings_migration.rb', 'db/migrate/create_settings.rb'
      if @settings[:create_redactor2_assets]
        migration_template 'create_redactor2_assets.rb', 'db/migrate/create_redactor2_assets.rb'
      end
    end

    def templates
      template 'deploy.rb.erb', 'config/deploy.rb'
      template 'database.yml', 'config/database.yml'
      template 'database.yml', 'config/database.yml.example'
      template 'admin.slim', 'app/views/layouts/admin.slim'
      template 'application.slim.erb', 'app/views/layouts/application.slim'
      template '_sidebar.slim.erb', 'app/views/shared/admin/_sidebar.slim'
    end

    def update_gitignore
      append_file '.gitignore', file_content('.gitignore')
    end

    def update_procfile
      if file_exists?('Procfile')
        append_file('Procfile', file_content('Procfile'))
      else
        copy_file 'Procfile', 'Procfile'
      end
    end

    def bundle
      run_in_root 'bundle'
    end

    def install_devise
      return unless @settings[:install_devise]
      generate 'devise:install'
      # create user
      generate 'devise User roles_mask:integer --routes=false'
      remove_file 'app/models/user.rb'
      copy_file 'install_devise/user.rb', 'app/models/user.rb'
      # add ability
      copy_file 'install_devise/ability.rb', 'app/models/ability.rb'
      # some settings
      gsub_file 'config/initializers/devise.rb', /.*please-change-me-at-config-initializers-devise@example.com\'/, "  config.mailer_sender = Settings.get.email_header_from || 'admin@molinos.ru' rescue nil"
      gsub_file 'config/initializers/devise.rb', /.*# config.omniauth .*/, "  config.omniauth :facebook, '436680133113363', '99303f2141cf42f1a4fd96e101933751'\n  config.omniauth :twitter, 'test', 'test'\n  config.omniauth :vkontakte, 'test', 'test'"
      inject_into_file 'config/application.rb', file_content('install_devise/application.rb'), after: /Rails::Application\n/
      # create adminos_user
      migration_template 'install_devise/populate_users_migration.rb', 'db/migrate/populate_users.rb'
    end

    def devise_views_locales
      return unless @settings[:add_devise_views_locales]
      directory 'devise_views_locales/devise', 'app/views/devise', mode: :preserve
      copy_file 'devise_views_locales/devise.ru.yml', 'config/locales/devise.ru.yml'
    end

    def install_paper_trail
      return unless @settings[:install_paper_trail]
      generate 'paper_trail:install --with-changes'
    end

    def add_pages
      return unless @settings[:add_pages]
      migration_template 'add_pages/pages_migration.rb', 'db/migrate/create_pages.rb'
      copy_file 'add_pages/page.rb', 'app/models/page.rb'
      copy_file 'add_pages/pages_controller.rb', 'app/controllers/pages_controller.rb'
      directory 'add_pages/views/pages', 'app/views/pages', mode: :preserve
    end

    def add_authentications
      return unless @settings[:add_authentications]
      migration_template 'add_authentications/authentications_migration.rb', 'db/migrate/create_authentications.rb'
      copy_file 'add_authentications/authentication.rb', 'app/models/authentication.rb'
      copy_file 'add_authentications/authentications_controller.rb', 'app/controllers/authentications_controller.rb'
      directory 'add_authentications/views/authentications', 'app/views/authentications', mode: :preserve
    end

    def add_adminos_users
      return unless @settings[:add_users_to_admin_panel]
      # admin controller
      copy_file 'admin_panel/users/admin_users_controller.rb', 'app/controllers/admin/users_controller.rb'
      # admin views
      directory 'admin_panel/users/views/admin_users', 'app/views/admin/users', mode: :preserve
    end

    def add_adminos_versions
      return unless @settings[:add_versions_to_admin_panel]
      # admin controller
      copy_file 'admin_panel/versions/admin_versions_controller.rb', 'app/controllers/admin/versions_controller.rb'
      # admin views
      directory 'admin_panel/versions/views/admin_versions', 'app/views/admin/versions', mode: :preserve
    end

    def add_adminos_pages
      return unless @settings[:add_pages]
      # admin controller
      copy_file 'admin_panel/pages/admin_pages_controller.rb', 'app/controllers/admin/pages_controller.rb'
      # admin views
      directory 'admin_panel/pages/views/admin_pages', 'app/views/admin/pages', mode: :preserve
    end

    def copy_route_config
      routes = []
      routes << "  # ADMINOS ROUTES STRAT"
      # Redactor2Rails
      routes << "  mount Redactor2Rails::Engine => '/redactor_rails'"
      # devise
      if @settings[:install_devise]
        routes << "\n  devise_for :users, skip: :omniauth_callbacks"
      end
      if @settings[:add_authentications]
        routes << "\n  devise_for :users, skip: [:session, :password, :registration, :confirmation],"
        routes << "    controllers: { omniauth_callbacks: 'authentications' }"
        routes << "\n  devise_scope :user do"
        routes << "    get 'authentications/new', to: 'authentications#new'"
        routes << "    post 'authentications/link', to: 'authentications#link'"
        routes << "  end"
      end
      # adminos
      routes << "\n  namespace :admin do"
      routes << "    resources :helps, only: :index"
      routes << "    resource  :settings, only: [:edit, :update]"
      if @settings[:add_users_to_admin_panel]
        routes << "\n    resources :users, except: :show do"
        routes << "      collection { post :batch_action }"
        routes << "    end"
      end
      if @settings[:add_versions_to_admin_panel]
        routes << "\n    resources :versions, only: [:index, :show] do"
        routes << "      collection { post :batch_action }"
        routes << "    end"
      end
      if @settings[:add_pages]
        routes << "\n    resources :pages, except: :show do"
        routes << "      collection { post :batch_action }"
        routes << "      member { put :drop }"
        routes << "      member { post :duplication }"
        routes << "    end"
        routes << "\n    root to: 'pages#index', as: :root"
      end
      routes << "  end"
      routes << "\n  root to: 'index#index'"
      # pages
      if @settings[:add_pages]
        routes << file_content('routes/pages.rb')
      end
      routes << "  # ADMINOS ROUTES END\n"
      inject_into_file 'config/routes.rb', routes.join("\n"), after: /routes.draw do\n/
    end

    def modify_application_config
      inject_into_file 'config/application.rb', "    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')\n", after: /Rails::Application\n/
      inject_into_file 'config/application.rb', "    config.action_mailer.default_url_options = { host: 'molinos.ru' }\n", after: /Rails::Application\n/
      inject_into_file 'config/application.rb', "    config.generators { |g| g.test_framework :rspec }\n", after: /Rails::Application\n/
      if @settings[:set_default_locale_time_zone]
        inject_into_file 'config/application.rb', "    config.time_zone = 'Moscow'\n", after: /Rails::Application\n/
        inject_into_file 'config/application.rb', "    config.i18n.default_locale = :ru\n", after: /Rails::Application\n/
      end
    end

    def locale_specific_actions
      unless options.locales?
        create_file 'config/locales/ru.yml', "ru:\n"
        return
      end

      inject_into_file 'config/application.rb', "    config.i18n.available_locales = #{options.locales.split(',').map { |s| s.strip.to_sym }.unshift(:ru)}\n", before: /.*i18n.default_locale.*\n/
      inject_into_file 'config/routes.rb', '  ', before: /  .*/, force: true
      inject_into_file 'config/routes.rb', "\n  scope '(:locale)', locale: /\#{I18n.available_locales.join('|')}/ do \n", before: /namespace/
      inject_into_file 'config/routes.rb', '  ', before: /namespace/, force: true
      inject_into_file 'config/routes.rb', "\n  end", after: /.*root to: 'index#index'/
      inject_into_file 'app/controllers/application_controller.rb', ', :set_locale', after: /.*before_action :reload_routes/
      inject_into_file 'app/controllers/application_controller.rb', file_content('locale/controller.rb'), after: /.*protected\n/
      inject_into_file 'app/views/shared/admin/_footer.slim', file_content('locale/locales.slim'), after: /.*wrapper\n/
      if @settings[:add_pages]
        migration_template 'locale/add_translation_table_to_page.rb', 'db/migrate/add_translation_table_to_page.rb'
        inject_into_file 'app/models/page.rb', file_content('locale/page.rb'), before: /.*BEHAVIORS = .*/
        comment_lines 'app/models/page.rb', /validates :name/
        inject_into_file 'app/models/page.rb', ', :translations_attributes', after: /.*:parent_id, :published/
        inject_into_file 'app/controllers/admin/pages_controller.rb', "            filter_by_locale: true,\n", after: /.*[:sorted],\n/
        inject_into_file 'app/controllers/application_controller.rb', "  before_action :check_page_name_locale, unless: -> { controller_path.split('/').first == 'admin' }", after: /.*before_action.*\n/
        remove_file 'app/views/admin/pages/_fields.slim'
      end
      directory 'locale/auto', '.'
    end

    private

    def run_in_root(command, **options)
      Bundler.with_clean_env { in_root { run(command, options) } }
    end

    def generate(generator, *args, force: options[:force])
      run_in_root "bin/rails generate #{generator} #{args.join(' ')} #{'--force' if options[:force]}"
    end

    def file_content(file)
      IO.read find_in_source_paths file
    end

    def file_exists?(file)
      File.exist?(file)
    end

    def ask_user(text)
      options.is_test? || yes?("#{text} (yN)")
    end
  end
end
