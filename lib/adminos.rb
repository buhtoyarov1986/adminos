require 'path'
require 'jquery-ui-rails'
require 'jquery-fileupload-rails'

module Adminos
  Helpers      = Module.new
  NestedSet    = Module.new
  Controllers  = Module.new
  StatefulLink = Module.new

  Path.require_tree 'adminos'

  class Engine < ::Rails::Engine
    initializer 'adminos.view_helpers' do
      ActionView::Base.send :include, Helpers::View
      ActionView::Base.send :include, Helpers::Admin
      ActionView::Base.send :include, Helpers::Bootstrap
    end

    initializer 'adminos.controller_helpers' do
      ActionController::Base.send :include, Controllers::Helpers
      ActionController::Base.send :include, Controllers::Resource
    end

    initializer 'adminos.stateful_link' do
      ActionView::Base.send :include, StatefulLink::Helper
      ActionController::Base.send :include, StatefulLink::ActionAnyOf
    end
  end
end
