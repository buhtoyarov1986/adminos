Gem::Specification.new do |gem|
  gem.name          = 'adminos'
  gem.version       = '5.2.0'
  gem.authors       = ['RavWar', 'milushov', 'abuhtoyarov', 'SiebenSieben']
  gem.email         = ['studio@molinos.ru']
  gem.homepage      = ''
  gem.summary       = 'Adminos'
  gem.description   = 'A framework for creating admin dashboards'

  gem.files         = `git ls-files`.split($/)
  gem.test_files    = gem.files.grep %r{^spec/}
  gem.require_paths = %w(lib)

  gem.add_dependency 'path'
  gem.add_dependency 'jquery-ui-rails', '~> 5.0.5'
  gem.add_dependency 'jquery-fileupload-rails'
  gem.add_dependency 'redactor-rails'
  gem.add_dependency 'railties', '~> 5.2'
  gem.add_development_dependency 'bundler', '~> 1.3'
  gem.add_development_dependency 'rails'
  gem.add_development_dependency 'rake'
end
